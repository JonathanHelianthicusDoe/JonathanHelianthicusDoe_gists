# Games &amp; their grouplike structures

## Commutative magma

### a.k.a. commutative nonassociative magma, a.k.a. abelian magma

| Game                | Other names for this game              | Underlying set                        | Binary operation                                                                                             | Misc. other properties  |
| :------------------ | :------------------------------------- | :------------------------------------ | :----------------------------------------------------------------------------------------------------------- | :---------------------- |
| rock-paper-scissors | scissors-rock-paper, roshambo, &amp;c. | The three possible moves in the game. | The &ldquo;winner&rdquo; operation, which results in whichever one of the operands wins or ties the matchup. | idempotent, alternative |

## Quasigroup

| Game   | Other names for this game      | Underlying set              | Binary operation                                                           | Misc. other properties                                                                              |
| :----- | :----------------------------- | :-------------------------- | :------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------- |
| sudoku | number place, 数字は独身に限る | {1, 2, 3, 4, 5, 6, 7, 8, 9} | Defined by interpreting a given completed sudoku puzzle as a Cayley table. | Due to the rules of the game, the sets {1, 2, 3}, {4, 5, 6}, and {7, 8, 9} are all generating sets. |

## Groupoid

### a.k.a. virtual group, a.k.a. Brandt groupoid

| Game      | Other names for this game                                             | Underlying set                             | Binary operation | Misc. other properties |
| :-------- | :-------------------------------------------------------------------- | :----------------------------------------- | :--------------- | :--------------------- |
| 15-puzzle | 8-puzzle, `n`-puzzle, gem puzzle, boss puzzle, mystic square, &amp;c. | Valid moves/transformations of the puzzle. | composition      |                        |

## Group

### a.k.a. nonabelian group

| Game               | Other names for this game         | Underlying set                           | Binary operation | Misc. other properties     |
| :----------------- | :-------------------------------- | :--------------------------------------- | :--------------- | :------------------------- |
| Rubik&rsquo;s Cube | hungarian magic cube, bűvös kocka | Valid moves/transformations of the cube. | composition      | subgroup of S<sub>48</sub> |

## Abelian group

### a.k.a. commutative group

| Game | Other names for this game | Underlying set                                                         | Binary operation                                                                           | Misc. other properties                                                                                                                                                                                                                                          |
| :--- | :------------------------ | :--------------------------------------------------------------------- | :----------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| nim  | 捡石子                    | All possible nim heaps, a.k.a. the nimbers, a.k.a. the Grundy numbers. | nim-sum, a.k.a. nimber addition, a.k.a. bitwise XOR, a.k.a. vector addition over **GF**(2) | Every nimber is its own inverse. This group is not finitely generated, but there is a homomorphism from this group to the (also abelian) two-element subgroup ({0, 1}, nim-sum) defined by simply taking the least significant (binary) digit of a nimber/heap. |

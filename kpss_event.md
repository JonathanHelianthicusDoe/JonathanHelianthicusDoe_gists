# Kiss-Pie-Storm-Safe (KPSS) Event!

We’re hosting an event to give Toontown players a chance to try out the [Kiss-Pie-Storm-Safe (KPSS)](https://kpss.neocities.org/) style of play! In a nutshell, this means never using any gags from the three “forbidden gag tracks”: sound, lure, and trap; see [the official website](https://kpss.neocities.org/) for more details. Anyone can participate, and we encourage anyone who enjoys the event and its playstyle to “join KPSS” and come play KPSS out in the wild!

## When?

2020-08-01, from 18:00 to 22:00 (all times listed in TTT, or ToonTown Time; this is the same thing as [Pacific Time](https://en.wikipedia.org/wiki/Pacific_Time_Zone)).

## Where?

Thwackville, [Toontown Rewritten](https://www.toontownrewritten.com/).

This event will also be livestreamed on at least [my Twitch channel](https://www.twitch.tv/jonathanhelianthicusdoe): <https://www.twitch.tv/jonathanhelianthicusdoe>.

## What?

- 18:00-19:00 | Sellbot HQ hour: VPs and (typically, long) factories
- 19:00-20:00 | Cashbot HQ hour: CFOs, coin mints, and bullion mints (and dollar mints, if you want!)
- 20:00-21:00 | Lawbot HQ hour: CJs, DA offices A, and DA offices D (and DA offices B and C, if you want!)
- 21:00-22:00 | Bossbot HQ hour: CEOs, front 3s, and back 9s (and middle 6s, if you want!)
- 22:00-??:?? | Post-event: Ask more questions, maybe even join [KPSS](https://kpss.neocities.org/)?

## Who?

Anyone! We accept all toons, so long as they agree not to bring nor use any gags from the forbidden three gag tracks: sound, lure, and trap. There are two exceptions:

- It’s OK to bring level 7 gags of the forbidden gag tracks, so long as you never use any of them.
- It’s OK to “stuff” your forbidden gag tracks when running CGCs (front 3s, middle 6s, and back 9s) in order to keep the golf minigame from giving you any gags of those tracks; again, so long as you never use any of them.

Of course, not all toons will be able to do all runs all the time: VPs, CFOs, CJs, CEOs, mints (except coin), DA offices (except A), and CGCs (except front 3) all have special requirements. For this reason, event-goers are encouraged to help out less fortunate toons by sometimes doing facilities that are open to everyone.

It is recommended to bring toons that have all four of the [KPSS](https://kpss.neocities.org/) gag tracks (toonup, throw, squirt, drop), but this is not a requirement.

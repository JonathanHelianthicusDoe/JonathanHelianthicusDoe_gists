# On sellbot field offices (SBFOs), especially concerning the problem of “difficulty” in Toontown

I’d like to start off by saying that SBFOs already satisfy the thing that I
think (and I’m sure many would agree) Toontown/TTR needs most, which is simply
new actual game-content. That being said, if I had to put an item #2 on the
list of things that Toontown/TTR needs most, it would be additional challenge
for the players. The fact is that it’s more or less trivial for many/most
players to achieve 110+ laff points, and yet the game gives no use for laff
points beyond having roughly 60 of them. And even then, you can do everything
in the game without breaking too much of a sweat, having merely 41+ or so laff
(ignoring the fact that you can’t actually enter, say, a CEO, with so little
laff). And this isn’t even to mention the fact that many of the game’s rewards
(SOSs, unites, level 7 gags, fires…) have no real use within normal gameplay
other than convenience (viz. making things go slightly faster).

Players (myself included) have already come up with a variety of ways of
artificially introducing difficulty into the game (not to be confused with the
term “artificial difficulty” in the context of game design, q.v.
[https://tvtropes.org/pmwiki/pmwiki.php/Main/FakeDifficulty](https://tvtropes.org/pmwiki/pmwiki.php/Main/FakeDifficulty)),
ranging from the time-honored tradition of making so-called “uber” toons (of
which I admit to having far too many), to banning gag tracks (see e.g.
[https://kpss.neocities.org/](https://kpss.neocities.org/), a shameless plug of
a ruleset that I came up with myself), to simply doing bosses with fewer toons
(e.g. 4 instead of 8), &c. A friend of mine has, in the same vein, suggested
that Toontown’s laff limit be lowered from 137 to ≈55…

## Problems for implementing “difficult” mechanics in SBFOs (and Toontown generally)

I know that SBFOs are (presumably) intended to fill this difficulty gap: “These
highly anticipated ‘boss buildings’ bring a new difficulty level to Toontown”.
However, I worry that this _could_ fall into one of the following traps:

* SBFOs are not that challenging for everyone, e.g. not so challenging for 137+
  laffers with 6 gag tracks.
* SBFOs may be challenging, but the challenge does not scale with both laff
  points and gags, e.g. they are roughly equally challenging for a 75 laffer
  and for a 132 laffer, and/or are roughly equally challenging for a toon with
  unmaxed gags and a toon with maxed gags. (Note that this is _not necessarily_
  meant to imply that so-called “semi” toons need to be at a very serious
  disadvantage.)
* SBFOs do not at least _somewhat_ encourage the use of special resources like
  fires, SOSs, unites, level 7 gags, &c.

The second point is perhaps the most subtle… for a simple example, it is easy
to introduce difficulty to all players and their toons by designing a minigame
that says to the player, “if you mess up two (or more) times on this minigame,
your toon will surely (or almost surely) go sad”. But it is easy to see that
this does not scale well; at best, if every time you mess up you take 64
damage, then a toon with 65 max laff certainly has an advantage over one with
64, and one with 129 over one with 128, but the nuance of this hypothetical
minigame ends there.

## Cogs are not (usually) scary

Another problem (and this applies to all three bullet points, not least the
second) is that cogs themselves are not that scary; the scariest sellbot is
typically a level 11 Mingler, with a level 12 Mr. Hollywood coming in a close
second. But fighting rows of four such cogs is tedious at best, assuming that
everyone has their gag tracks maxed, and making them v2.0 cogs ups the
challenge largely by just making it even more tedious (I am reminded again of
the term “artificial difficulty”…). Now, if you asked me, I would say that this
is in large part because two gag tracks in particular are not exactly balanced
(viz. sound & lure), and indeed that was my motivation for coming up with
KPSS-style gameplay. But there are other reasons that seem actually fixable (as
I don’t seriously propose that sound & lure be removed from the game at the
mechanical level!). One such reason is very simply that cogs need to go to
higher levels. Cogs are designed to scale up roughly quadratically (“roughly”
due to the exception of level 12s having a bonus 10%) in terms of HP and
roughly linearly in terms of damage output (that is, ability to damage toons).
The ability to damage toons is surely the most important, for reasons that are
hopefully obvious at this point, but higher level (that is, higher than level
12) cogs having more HP is _also_ important. This is not simply to make the
fights more tedious (although this is, depending on who you ask, an unfortunate
side-effect), but helps to balance sound gags generally, and helps to make
fights more interesting by introducing new combinations/strategies w.r.t. gag
usage.

## Toon-cog asymmetry: intentional, but leaves something to be desired

The other reason that I have in mind as to why cogs are not scary is that,
while cog fights _appear_ more-or-less symmetrical at first blush, they feature
some rather odd asymmetries that make toon laff points scale _much_ worse than
cog HP, and make fights much more transparent for toons than they are for cogs.
Of course, many, even most, of these asymmetries are directly intentional, but
it is nevertheless worth investigating them and re-evaluating how they could be
exploited or changed to make new Toontown content that is challenging in a
novel way.

Most of these asymmetries are located in the (not-so-)similarity between toons’
gags and cogs’ attacks. Again, at first blush these look similar: the two sides
take turns, and when it is one side’s turn, each character (be it a cog or a
toon, depending on which side’s turn it is) performs their chosen attack in
attempt to defeat the other side; each such attack has a random chance of
missing. The first obvious asymmetry here is fairly innocent, namely that a cog
attack does (in general) a smaller portion of a toon’s laff than a gag does to
a cog (in terms of a proportion of that cog’s max HP). This is fairly innocent
because the intent here is that _one_ toon defeats, in one “go”, _many_ cogs.
There just are more cogs than there are toons, which is fine. Unfortunately,
from here the asymmetries only deepen more rapidly:

* More than one toon can attack at the same time (viz. if they all use gags
  from a given single gag track); this is not true of cogs, and thus cogs can
  make no use of “yellow damage” or anything vaguely of that nature (use your
  imagination here), since their attacks are always done in series.
* Cogs cannot heal themselves nor one another, but toons have a number of ways
  of doing this at their disposal (primarily toonup gags, of course).
* Every attack that a given cog species at a given level (e.g. level 11
  Mingler) can perform does roughly the same amount of damage, with the slight
  exception of cogs like e.g. Glad Handers, which have attack(s) that are
  effectively automatic misses (since taking a damage instance of less than
  five or so is rarely distinguishable from a miss, i.e. zero damage). This is
  in stark contrast to gags, on two counts. One is that gags simply tend to do
  a wide variety of damage amounts, from the 27 of a fruit pie, to the 60 of a
  safe, to the 100 of a cake, to the 180 of a TNT, &c. The other is that toon
  gags that do damage to multiple targets (barring level 7 gags, this is just
  to say “sound gags”) do significantly less damage per-gag-per-target than
  other gags, while this is very much not true of single-target cog attacks vs.
  multi-target cog attacks, which tend to do roughly the same amount of damage
  per-gag-per-target.
* Particularly relevant to the previous point is that cogs choose their attacks
  in a wildly different way than toons do. This is not just to say that cogs
  essentially have a fixed probability assigned to each attack, whereas toons
  are controlled by humans (although this is a particular asymmetry that could
  be lessened by giving the cogs a more sophisticated decision procedure). This
  is mainly to say that toons can only carry a fixed number of each kind of
  gag, whereas cogs have an unlimited number of each attack in their arsenal.
  This is not to suggest that perhaps _all_ cog attacks be limited, as it makes
  perfect sense for every cog to have at least one attack that they can use
  infinitely many times, but perhaps cogs having one-time- (or two-time-, or…)
  use attacks as well would make sense.
* Toons can manipulate cogs to some extent, but the reverse is not true. Mainly
  this comes in the form of lure gags/SOSs, but also includes things like “cogs
  always miss” SOSs and, arguably, the ability for toons to “stun” cogs in
  order to lower the cogs’ chance of dodging attacks.

All of this is merely to give suggestions and food for thought in lieu of
making SBFOs really “bring a new difficulty level to Toontown”. Onto that I’d
like to just add a few extra notes:

## Extra notes

Since the implementation of SBFOs is going to involve (a) new toontask line(s),
I’d like to express at least one concern that I have about said toontasks,
given that I think toontasks are an important part of the game (an opinion that
I grant may prove unpopular, depending on which particular group of toons you
ask). The toontask line immediately preceding the SBFO tasks (that is, the
bossbot suit task, currently the last toontask line in the game) is, while
excusable due to it being a toontask line from the original TTO, rather
embarrassing. I’m fairly sure that just about no one actually likes this task
line, as it is needlessly tedious (many toons by this point have maxed their
gags, and thus have no real reason to grind front threes other than for the
task), and comes off as unimaginative and with little supposed justification
(“I need you to investigate this new cog thing…” was not exactly a new premise
for a toontask by the time that BBHQ was added to TTO, and the repeated need to
defeat one v2.0 cog at a time comes with basically zero dialogue or any other
attempt at justification). I just hope that the SBFO-related toontask line(s)
do not follow in the same footsteps. And, while we’re at it, it might even be
cool to allow toons to make choices within the tasks themselves, just to create
a bit of a branching tree structure of possible toontask lines that toons can
choose from.

Also, it would be nice to have (at least) the following eight SBFO-related
strings added to the Speedchat+ whitelist:

* `fieldoffice`
* `fieldoffices`
* `sbfo`
* `sbfos`
* `sbfo's`
* `sfo`
* `sfos`
* `sfo's`

---

— Dr. Jonathan Helianthicus Doe, Ⅳ (TTID-BFMQ-JMCB)
